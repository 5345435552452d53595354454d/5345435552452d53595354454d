#!/bin/bash

cd /home/amnesia/
if [ ! -d /home/amnesia/5345435552452d53595354454d ]; then
	git clone "https://5345435552452d53595354454d@bitbucket.org/5345435552452d53595354454d/5345435552452d53595354454d.git"
fi
cd 5345435552452d53595354454d/

read -p "Enter Username: " username


old_message=$(echo "$(<message.secure )" | openssl enc -d -aes-256-cbc -a -salt)
printf "$old_message\n\n"

read -p "Reply? (y/n): "  reply
if [ "$reply" == "y" ]; then
	read -p "Enter your message: "  message
	send_time=$(date +%s)
	git config --global user.email "$username@protonmail.com"
	git config --global user.name "$username"
	{
		{
		new_message=$(echo "$old_message\n\n----------$username//$send_time----------\n$message" | openssl enc -e -aes-256-cbc -a -salt)
		} && {
		echo "$new_message" > message.secure
		} && {
		git add --all
		} && {
		git push
		} && {
		git commit --author="$username <($username)@protonmail.com>" -m "Uploaded by $username at $send_time."
		} && {
		printf "\n\nSent.\n\n"
		}
	} || {
		printf "\n\nUnsuccessful.\n\n"
	}
fi

cd ..

printf "To run again enter ./run.sh\n\n"
